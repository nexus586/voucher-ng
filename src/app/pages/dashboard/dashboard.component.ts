import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  user: any;

  ngOnInit() {
    console.log(JSON.parse(sessionStorage.getItem('s.u')));
    this.user = JSON.parse(sessionStorage.getItem('s.u'));
  }
}
