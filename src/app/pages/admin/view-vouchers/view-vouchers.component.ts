import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../../shared/app.services';

@Component({
  selector: 'app-view-vouchers',
  templateUrl: './view-vouchers.component.html',
  styleUrls: ['./view-vouchers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewVouchersComponent implements OnInit {

  vouchers: Object;
  loading: boolean;

  constructor(private service: AppService) { }

  ngOnInit() {
    this.loading = true;
    this.service.findAllVouchers()
    .subscribe(
      (data) => {
        this.loading = false;
        this.vouchers = data;
      },
      (error) => {
        this.loading = false;
        console.log('An error occurred');
      }
    )
}

}
