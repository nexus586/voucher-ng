import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../../shared/app.services';

@Component({
  selector: 'app-view-transactions',
  templateUrl: './view-transactions.component.html',
  styleUrls: ['./view-transactions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewTransactionsComponent implements OnInit {

  loading: boolean;
  payments: Object;

  constructor(private service: AppService) { }

  ngOnInit() {
    this.loading = true;
    this.service.findAllPayments()
      .subscribe(
        (data) => {
          this.loading = false;
          this.payments = data;
        },
        (error) => {
          this.loading = false;
          console.log('An error occurred');
        }
      );
  }

}
