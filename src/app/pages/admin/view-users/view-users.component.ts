import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../../shared/app.services';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewUsersComponent implements OnInit {

  users: Object;
  loading: boolean;
  IUser: any;


  constructor(private service: AppService, private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.service.findAllUsers()
      .subscribe(
        (data) => {
          this.loading = false;
          this.users = data;
        },
        (error) => {
          this.loading = false;
        }
      );
  }

  activateUser(id: number) {
    this.IUser = {
      id: id
    };

    // console.log('[user data is] =>' + JSON.stringify(this.IUser));



    this.service.activateUser(JSON.stringify(this.IUser))
      .subscribe(
        (data) => {
          swal({
            title: 'Success',
            text: 'User activated',
            type: 'success',
            // showCancelButton: true,
            // cancelButtonText: 'Close'
          })
          setTimeout(() => {
            this.router.navigate(['/', 'pages', 'admin', 'view-users']);
          }, 3000);
        },
        (error) => {
          swal({
            title: 'Error',
            text: 'User already activated',
            type: 'error',
            // showCancelButton: true,
            // cancelButtonText: 'Close'
          })
        }
      );
  }

}
