import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { ViewUsersComponent } from './view-users/view-users.component';
import { ViewVouchersComponent } from './view-vouchers/view-vouchers.component';
import { ViewTransactionsComponent } from './view-transactions/view-transactions.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
  declarations: [ViewUsersComponent, ViewVouchersComponent, ViewTransactionsComponent]
})
export class AdminModule { }
