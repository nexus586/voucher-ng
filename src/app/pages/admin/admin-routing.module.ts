import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewVouchersComponent } from './view-vouchers/view-vouchers.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { ViewTransactionsComponent } from './view-transactions/view-transactions.component';

const routes: Routes = [
  {
    path: 'view-vouchers',
    component: ViewVouchersComponent,
    data: {breadcrumb: 'View Vouchers'}
  },
  {
    path: 'view-users',
    component: ViewUsersComponent,
    data: {breadcrumb: 'View Users'}
  },
  {
    path: 'view-payments',
    component: ViewTransactionsComponent,
    data: {breadcrumb: 'View Payments'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
