import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InitiatePaymentComponent } from './initiate-payment/initiate-payment.component';
import { VerifyVoucherComponent } from './verify-voucher/verify-voucher.component';
import { ViewPaymentsComponent } from './view-payments/view-payments.component';

const routes: Routes = [
  {
    path: 'initiate-payment',
    component: InitiatePaymentComponent,
    data: {breadcrumb: 'Initiate Payment'}
  },
  {
    path: 'verify-voucher',
    component: VerifyVoucherComponent,
    data: {breadcrumb: 'Verify Voucher'}
  },
  {
    path: 'view-payments',
    component: ViewPaymentsComponent,
    data: {breadcrumb: 'View Payments'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantRoutingModule { }
