import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../../shared/app.services';
import swal from 'sweetalert2';

@Component({
  selector: 'app-verify-voucher',
  templateUrl: './verify-voucher.component.html',
  styleUrls: ['./verify-voucher.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VerifyVoucherComponent implements OnInit {


  form: FormGroup;
  voucherCode = new FormControl('', Validators.required);
  IVoucher: any;

  constructor(private router: Router, private fb: FormBuilder, private service: AppService) { }

  ngOnInit() {
    this.form = this.fb.group({
      voucherCode: this.voucherCode
    });
  }

  submit(): void {
    this.IVoucher = {
      voucherCode: this.form.value.voucherCode
    };

    swal({
      title: 'Verifying',
      text: 'Please wait while the voucher is verified',
      type: 'info',
      timer: 1000
  });

    console.log(JSON.stringify(this.IVoucher));

    this.service.verifyVoucher(JSON.stringify(this.IVoucher))
      .subscribe(
        (data) => {
          swal({
            title: 'Success',
            text: 'Voucher Verified. You can now initiaite payment',
            type: 'success'
          });
          setTimeout(() => {
            this.router.navigate(['/', 'pages', 'merchant', 'initiate-payment']);
          }, 2000);
        },
        (error) => {
          swal({
            title: 'Error',
            text: 'Voucher could not be verified',
            type: 'error'
          });
          console.log('[error] => ', error);
        }
      )
  }

}
