import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from '../../shared/app.services';
import { Router } from '@angular/router';
// import { CustomValidators } from 'ng2-validation';
import swal from 'sweetalert2';

@Component({
  selector: 'app-initiate-payment',
  templateUrl: './initiate-payment.component.html',
  styleUrls: ['./initiate-payment.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InitiatePaymentComponent implements OnInit {

  form: FormGroup;
  payment: any;
  currentUser: any;

  voucherCode = new FormControl('', Validators.required);
  itemBought = new FormControl('', Validators.required);
  clientAccountNumber = new FormControl('', Validators.required);
  price = new FormControl('', Validators.required);
  quantity = new FormControl('', Validators.required);

  constructor(private fb: FormBuilder, private service: AppService, private router: Router) { }

  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.getItem('s.u'));

    this.form = this.fb.group({
      voucherCode: this.voucherCode,
      itemBought: this.itemBought,
      clientAccountNumber: this.clientAccountNumber,
      price: this.price,
      quantity: this.quantity
    });
  }

  submit(): void {
    this.payment = {
      itemBought: this.form.value.itemBought,
      voucherCode: this.form.value.voucherCode,
      price: this.form.value.price,
      quantity: this.form.value.quantity,
      clientAccountNumber: this.form.value.clientAccountNumber
    }

    console.log(JSON.stringify(this.payment));

    swal({
      title: 'Please Wait',
      text: 'Processing Transaction',
      type: 'info',
      timer: 1000
    })

    this.service.initiatePayment(JSON.stringify(this.payment), this.currentUser.id)
      .subscribe(
        (data) => {
          swal({
            title: 'Success',
            text: 'Payment made',
            type: 'success',
            timer: 2000
          });
          console.log('[data] => ', data);
          setTimeout(() => {
            this.router.navigate(['/', 'pages', 'merchant', 'view-payments']);
          }, 2000);
        },
        (error) => {
          swal({
            title: 'Error',
            text: 'Sorry and error occurred',
            type: 'error',
            timer: 2000
          });
          console.log('[error] => ', error);
        }
      )
  }

}
