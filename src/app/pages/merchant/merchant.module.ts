import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MerchantRoutingModule } from './merchant-routing.module';
import { VerifyVoucherComponent } from './verify-voucher/verify-voucher.component';
import { ViewPaymentsComponent } from './view-payments/view-payments.component';
import { InitiatePaymentComponent } from './initiate-payment/initiate-payment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MerchantRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [VerifyVoucherComponent, ViewPaymentsComponent, InitiatePaymentComponent]
})
export class MerchantModule { }
