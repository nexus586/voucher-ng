import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../../shared/app.services';

@Component({
  selector: 'app-view-payments',
  templateUrl: './view-payments.component.html',
  styleUrls: ['./view-payments.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewPaymentsComponent implements OnInit {

  payments: Object;
  loading: boolean;

  user: any;
  userId: number;
  constructor(private service: AppService) { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('s.u'));
    this.userId = this.user.id;
    this.loading = true;
    this.service.findAllMerchantPayments(this.userId)
      .subscribe(
        (data) => {
          this.loading = false;
          this.payments = data;
          console.log('[data] ==> ', data);
        },
        (error) => {
          this.loading = false;
          console.log('[Error] ==>', error);
        }
      )
  }

}
