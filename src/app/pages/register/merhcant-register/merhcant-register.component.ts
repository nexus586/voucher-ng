import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Router } from '@angular/router';
import { AppService } from '../../shared/app.services';
import swal from 'sweetalert2';

@Component({
  selector: 'app-merhcant-register',
  templateUrl: './merhcant-register.component.html',
  styleUrls: ['./merhcant-register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MerhcantRegisterComponent implements OnInit {

  form: FormGroup;
  name = new FormControl('', [Validators.required, Validators.minLength(3)]);
  email = new FormControl('', [Validators.required, Validators.email]);
  phone = new FormControl('', [Validators.required, Validators.minLength(10)]);
  password = new FormControl('', [Validators.required, Validators.minLength(6)]);
  confirmPassword = new FormControl('', [Validators.required, Validators.minLength(6), CustomValidators.equalTo(this.password)]);
  accountNumber = new FormControl('', [Validators.required]);
  register: any;

  constructor(private router: Router, private fb: FormBuilder, private service: AppService) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: this.name,
      email: this.email,
      phone: this.phone,
      password: this.password,
      confirmPassword: this.confirmPassword,
      accountNumber: this.accountNumber
    });
  }

  public onSubmit(): void {
    this.register = {
      email: this.form.value.email,
      name: this.form.value.name,
      password: this.form.value.password,
      phone: this.form.value.phone,
      accountNumber: this.form.value.accountNumber
    }

    swal({
      title: 'Registration',
      text: 'Please wait while your account is created',
      type: 'info',
      timer: 1000
    });

    console.log('[merchant] => ' + JSON.stringify(this.register));

    this.service.doMerchantRegistration(JSON.stringify(this.register))
      .subscribe(
        (data) => {
          swal({
            title: 'Success',
            text: 'Registration successful. Please wait for an administrator to activate your account',
            type: 'success'
          });

          setTimeout(() => {
            this.router.navigate(['/', 'login']);
          }, 2000);
        },
        (error) => {
          swal({
            title: 'Error',
            text: 'Something went wrong, please try again later',
            type: 'error'
          });
        }
      );
  }

  ngAfterViewInit() {
    document.getElementById('preloader').classList.add('hide');
  }

}
