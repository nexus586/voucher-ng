import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AppService } from './../shared/app.services';
import swal from 'sweetalert2';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {

    form: FormGroup;
    name = new FormControl('', [Validators.required, Validators.minLength(3)]);
    email = new FormControl('', [Validators.required, Validators.email]);
    phone = new FormControl('', [Validators.required, Validators.minLength(10)]);
    password = new FormControl('', [Validators.required, Validators.minLength(6)]);
    confirmPassword = new FormControl('', [Validators.required, Validators.minLength(6), CustomValidators.equalTo(this.password)]);
    accountNumber = new FormControl('', Validators.required);
    register: any;

    constructor(private router: Router, private fb: FormBuilder, private service: AppService) { }

    ngOnInit() {
        this.form = this.fb.group({
            name: this.name,
            email: this.email,
            phone: this.phone,
            password: this.password,
            confirmPassword: this.confirmPassword,
            accountNumber: this.accountNumber
        });
    }

    public onSubmit(): void {
        this.register = {
            email: this.form.value.email,
            name: this.form.value.name,
            phone: this.form.value.phone,
            password: this.form.value.password,
            accountNumber: this.form.value.accountNumber
        }

        swal({
            title: 'Registration',
            text: 'Please wait while your account is created',
            type: 'info',
            timer: 1000
        });

        this.service.doClientRegistration(JSON.stringify(this.register))
            .subscribe(
                (data) => {
                    swal({
                        title: 'Registration',
                        text: 'Registration Successful. An administrator will activate your account soon',
                        type: 'success'
                    });
                },
                (error) => {
                    swal({
                        title: 'Error',
                        text: 'Unable to create account',
                        type: 'error'
                    });
                    console.log(error);
                }
            );
    }

    ngAfterViewInit() {
        document.getElementById('preloader').classList.add('hide');
    }
}

export function emailValidator(control: FormControl): { [key: string]: any } {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}

export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
        let password = group.controls[passwordKey];
        let passwordConfirmation = group.controls[passwordConfirmationKey];
        if (password.value !== passwordConfirmation.value) {
            return passwordConfirmation.setErrors({ mismatchedPasswords: true })
        }
    }
}
