import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AppService } from '../shared/app.services';
import swal from 'sweetalert2';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

    form: FormGroup;
    email = new FormControl('', [Validators.required, Validators.email]);
    password = new FormControl('', [Validators.required, Validators.minLength(6)]);
    login: any;

    ngOnInit(): void {
        // console.log('Login component has been called');

        this.form = this.fb.group({
            email: this.email,
            password: this.password
        });
    }

    constructor(private router: Router, private fb: FormBuilder, private service: AppService) { }

    public onSubmit() {
        this.login = {
            email: this.form.value['email'],
            password: this.form.value['password']
        }

        swal({
            title: 'Login',
            text: 'Please wait while we log you in',
            timer: 1000,
            type: 'info'
        });

        console.log(JSON.stringify(this.login));

        this.service.doLogin(JSON.stringify(this.login))
            .subscribe(
                (response) => {
                    console.log(response);
                    sessionStorage.setItem('s.u', JSON.stringify(response));
                    swal({
                        title: 'Success',
                        text: 'Logged In Successfully',
                        type: 'success',
                        timer: 2000
                    });
                    setTimeout(() => {
                        this.router.navigate(['/', 'pages', 'dashboard']);
                    }, 3000)
                },
                (error) => {
                    swal({
                        title: 'Error',
                        text: 'Unable to log in',
                        type: 'error'
                    });
                    console.log(error);
                }
            );
    }

    ngAfterViewInit() {
        document.getElementById('preloader').classList.add('hide');
    }

}
