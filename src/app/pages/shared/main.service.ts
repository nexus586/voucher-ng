import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class MainService {

  constructor(private http: HttpClient) {}

  get(url: string) {
    return this.http.get(url).map(response => {
      return response;
    });
  }

  post(url: string, data: any) {
    return this.http.post(url, data).map(response => {
      return response;
    });
  }

  put(url: string, data: any) {
    return this.http.put(url, data).map(response => {
      return response;
    });
  }
}
