import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        'Content-Type': 'application/json'
      }
    });

    // add authorization header with jwt token if available
    const currentUser = JSON.parse(sessionStorage.getItem('s.u'));
    console.log('indie jwt intercepter');
    // console.log('[token] =>' + currentUser.token);
    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          Authorization : `${currentUser.token}`
        }
      });
    }
    console.log('Authorization is ==> ' + request.headers.get('authorization'));
    // console.log('Current user is ==> ', currentUser.name);
    console.log('[Current request] ==> ', request);
    return next.handle(request);
  }
}

export let jwtInterceptor = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: JwtInterceptor,
  multi: true
};
