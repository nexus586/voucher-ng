import { Injectable } from '@angular/core';
import { MainService } from './main.service';
// import { NewBankUser, LoginModel, RetailSignupModel } from "../models/app.models";
import { AppConstants } from './app.constants';

@Injectable()
export class AppService {
  constructor(private mainService: MainService) { }

  // Login ...
  doLogin(loginModel: any) {
    return this.mainService.post(AppConstants.BASE_URL + 'user/login', loginModel);
  }
  // End Login ...

  // Client Registration
  doClientRegistration(model: any) {
    return this.mainService.post(AppConstants.BASE_URL + 'client/register', model);
  }
  // End Client Registration

  // Merchant Registration
  doMerchantRegistration(model: any) {
    return this.mainService.post(AppConstants.BASE_URL + 'merchant/register', model);
  }
  // End Merchant Registration

  /**
   * Admin Actions
   */
  // activate users start
  activateUser(IUser: any) {
    return this.mainService.post(AppConstants.BASE_URL + 'admin/activateUser', IUser);
  }
  // end activate users

  // find users start
  findAllUsers() {
    return this.mainService.get(AppConstants.BASE_URL + 'admin/findAllUsers');
  }
  // end find users

  // find payments start
  findAllPayments() {
    return this.mainService.get(AppConstants.BASE_URL + 'admin/viewAllPayments');
  }
  // end find payments

  // find vouchers start
  findAllVouchers() {
    return this.mainService.get(AppConstants.BASE_URL + 'admin/findAllVouchers');
  }
  // end find payments

  /**
   * Merchant actions
   */
  findAllMerchantPayments(id: number) {
    return this.mainService.get(AppConstants.BASE_URL + `merchant/main/findAllPayments?id=${id}`);
  }

  verifyVoucher(voucherCode: any) {
    return this.mainService.post(AppConstants.BASE_URL + 'merchant/main/verify', voucherCode);
  }

  initiatePayment(payment: any, id: number) {
    return this.mainService.post(AppConstants.BASE_URL + `merchant/main/initiatePayment?id=${id}`, payment);
  }

  /**
   * Client Actions
   */
  generateVoucher(voucher: any, id: number) {
    return this.mainService.post(AppConstants.BASE_URL + `client/main/generateVoucher?id=${id}`, voucher);
  }

  findAllClientPayments(id: number) {
    return this.mainService.get(AppConstants.BASE_URL + `client/main/findAllPayments?id=${id}`)
  }

  findAllClientVouchers(id: number) {
    return this.mainService.get(AppConstants.BASE_URL + `client/main/findAllVouchers?id=${id}`)
  }

}
