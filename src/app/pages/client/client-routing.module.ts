import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenerateVoucherComponent } from './generate-voucher/generate-voucher.component';
import { ViewVouchersComponent } from './view-vouchers/view-vouchers.component';
import { ViewPaymentsComponent } from './view-payments/view-payments.component';

const routes: Routes = [
  {
    path: 'generate-voucher',
    component: GenerateVoucherComponent,
    data: { breadcrumb: 'Generate Voucher' }
  },
  {
    path: 'view-vouchers',
    component: ViewVouchersComponent,
    data: { breadcrumb: 'View Vouchers' }
  },
  {
    path: 'view-payments',
    component: ViewPaymentsComponent,
    data: { breadcrumb: 'View Payments' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
