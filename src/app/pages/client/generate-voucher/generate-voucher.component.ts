import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppService } from '../../shared/app.services';


@Component({
  selector: 'app-generate-voucher',
  templateUrl: './generate-voucher.component.html',
  styleUrls: ['./generate-voucher.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenerateVoucherComponent implements OnInit {


  form: FormGroup;
  voucher: any;
  user: any;
  description = new FormControl('', Validators.required);
  amount = new FormControl('', Validators.required);

  constructor(private fb: FormBuilder, private router: Router, private service: AppService) { }

  ngOnInit() {
    this.form = this.fb.group({
      // voucher: this.voucher,
      description: this.description,
      amount: this.amount
    });

    this.user = JSON.parse(sessionStorage.getItem('s.u'));
  }

  submit() {
    this.voucher = {
      amount: this.form.value.amount,
      description: this.form.value.description
    };

    swal({
      title: 'Please wait',
      text: 'Voucher is being generated',
      type: 'info',
      timer: 2000
    });

    this.service.generateVoucher(JSON.stringify(this.voucher), this.user.id)
      .subscribe(
        (data) => {
          swal({
            title: 'Success',
            text: 'Voucher Generated',
            type: 'success',
            timer: 2000
          });

          setTimeout(() => {
            this.router.navigate(['/', 'pages', 'client', 'view-vouchers']);
          }, 2000);
          console.log(data);
        },
        (error) => {
          swal({
            title: 'Error',
            text: 'Voucher could not be generated',
            type: 'error',
            timer: 2000
          });

          console.log(error);
        }
      )
    // console.log('[Voucher form] ==> ' + JSON.stringify(this.voucher));
  }

}
