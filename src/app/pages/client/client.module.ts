import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { GenerateVoucherComponent } from './generate-voucher/generate-voucher.component';
import { ViewVouchersComponent } from './view-vouchers/view-vouchers.component';
import { ViewPaymentsComponent } from './view-payments/view-payments.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ClientRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [GenerateVoucherComponent, ViewVouchersComponent, ViewPaymentsComponent],
})
export class ClientModule { }
