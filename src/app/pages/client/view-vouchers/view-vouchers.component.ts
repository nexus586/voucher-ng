import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../../shared/app.services';

@Component({
  selector: 'app-view-vouchers',
  templateUrl: './view-vouchers.component.html',
  styleUrls: ['./view-vouchers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewVouchersComponent implements OnInit {

  vouchers: Object;
  loading: boolean;
  user: any;

  constructor(private service: AppService) { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('s.u'));
    this.loading = true;
    this.service.findAllClientVouchers(this.user.id)
      .subscribe(
        (data) => {
          this.loading = false;
          this.vouchers = data;
          console.log(data);
        },
        (error) => {
          this.loading = false;
        }
      )
  }

}
